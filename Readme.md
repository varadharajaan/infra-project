# ECS CLUSTER DEPLOYMENT WITH TERRAFORM

This repository contains the Terraform modules for creating a production ready ECS in AWS.

* [Bugs/Known Issues and Fixes](#bugsknown-issues and Fixes)
* [ECS infrastructure in AWS](#ecs-infra)
* [How to create the infrastructure](#create-it)
* [deployment](#must-know)
  * [Multi-account Deployment](#Multi-account Deployment)


## Bugs/Known Issues and Fixes

* The Amazon Machine Image (AMI) lookup is limited to Amazon Linux. - as a work around i have used Ubuntu AMI (works fine :) )
* ALB goes UnReachable sometimes which is mainly due to memory issues and EC2 instance type. (503 ERROR service unreachable) , However the ALB comes up in next few seconds.

## ECS infra

![ECS infra](img/ecs-infra.png)

What are we creating:

* VPC with a /16 ip address range and an internet gateway
* We are choosing a region and a number of availability zones we want to use. For high-availability we need at least two
* In every availability zone we are creating a private and a public subnet with a /24 ip address range
  * Public subnet convention is 10.x.0.x and 10.x.1.x etc..
  * Private subnet convention is 10.x.50.x and 10.x.51.x etc..
* In the public subnet we place a NAT gateway and the LoadBalancer
* The private subnets are used in the autoscale group which places instances in them
* We create an ECS cluster where the instances connect to


### Module structure

![Terraform module structure](img/ecs-terraform-modules.png)

## Create it

To create a working ECS cluster from this repository see **ecs.tf** and **${ENVIRONMENT}.tfvars**.

Quick way to create this from the repository as is:

```bash
terraform init && terraform apply -input=false -var-file=ecs.tfvars
```

Actual way for creating everything using the default terraform flow:

```bash
terraform init
terraform plan -input=false -var-file=${ENVIRONMENT}.tfvars
terraform apply -input=false -var-file=${ENVIRONMENT}.tfvars
```

## deployment

### Multi-account Deployment

* Master branch pipeline will get deployed to production
* Develop  branch pipeline will get deployed to Staging 
* Any other branch pipeline will get deployed to sandbox
* Destory Job is able to trigger when Variable ENVIORNMENT and DESTORY is provided
