terraform {
  backend "s3" {
    bucket = "tf-service"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}