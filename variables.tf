variable "environment" {
  type = string
  default = "production"
  description = "Options: development, qa, staging, production"
}

variable "AWS_REGION" {
  default = "eu-west-1"
}



variable "cidr_ab" {
  type = map
  default = {

    sandbox         = "10.0"
    staging         = "10.0"
    production      = "10.0"
  }
}

variable "instance_type" {
  default = "t2.small"
}

variable "app_port" {
  default = "8080"
}

variable "max_private_subnet_count" {
  default = 3
}

variable "max_public_subnet_count" {
  default = 3
}

variable "private_subnet_start" {
  default = 1
}

variable "public_subnet_start" {
  default = 101
}

variable "vpc_enable_nat_gateway" {
  default = false
}

variable "vpc_enable_vpn_gateway" {
  default = false
}

variable "app_name" {
  default = "my-service"
}

variable "app_version" {
  default = "latest"
}

variable "log_group" {
  default = "my-log-group"
}

variable "vpc_name" {
  default = "vpc"
}

variable "cluster_name" {
  default = "my-ecs"
}

variable "alb_name" {
  default = "my-alb"
}