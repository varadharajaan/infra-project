module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.7.0"
  name = var.vpc_name
  cidr                 = "${lookup(var.cidr_ab, var.environment)}.0.0/16"
  azs                  = local.availability_zones
  private_subnets      = local.private_subnets
  public_subnets       = local.public_subnets
  enable_nat_gateway = var.vpc_enable_nat_gateway
  enable_vpn_gateway = var.vpc_enable_vpn_gateway
  tags = {
    Terraform   = "true"
    Environment = "production"
  }
}

locals {
  private_subnets = [
  for az in local.availability_zones :
  "${lookup(var.cidr_ab, var.environment)}.${local.cidr_c_private_subnets + index(local.availability_zones, az)}.0/24"
  ]
  public_subnets = [
  for az in local.availability_zones :
  "${lookup(var.cidr_ab, var.environment)}.${local.cidr_c_public_subnets + index(local.availability_zones, az)}.0/24"
  ]
}

data "aws_availability_zones" "available" {
  state = "available"
}

locals {
  availability_zones = data.aws_availability_zones.available.names
}

locals {
  cidr_c_private_subnets  = var.private_subnet_start
  cidr_c_public_subnets   = var.public_subnet_start
  max_private_subnets     = var.max_private_subnet_count
  max_public_subnets      = var.max_public_subnet_count
}